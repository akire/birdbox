<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectInvitationRequest;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;

class ProjectInvitationsController extends Controller
{
    public function store(ProjectInvitationRequest $request, Project $project)
    {
    	// $this->authorize('update', $project);
    	// request()->validate([
    	// 	'email' => ['required', 'exists:users,email']
    	// ], [
    	// 	'email.exists' => 'The user you are inviting must have a Birdboard account.'
    	// ]);

    	// if(auth()->user() != $project->user) {
    	// 	return abort(403);
    	// }

    	$user = User::whereEmail(request('email'))->first();
    	$project->invite($user);
    	return redirect($project->path());
    }
}
