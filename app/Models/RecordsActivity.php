<?php

namespace App\Models;

use Illuminate\Support\Arr;

trait RecordsActivity
{
	public $oldAttributes = [];

	public static function bootRecordsActivity()
	{
		


		foreach (self::recordableEvents() as $event)
		{
			static::$event(function ($model) use ($event) {
				// $description = $event;

				// if (class_basename($model) !== 'Project')
				// {
				// 	$description = "{$event}_" . strtolower(class_basename($model));
				// }
				$model->recordActivity($model->activityDescription($event));
			});

			if ($event === 'updated') {
				static::updating(function ($model) {
					$model->oldAttributes = $model->getOriginal();
				});
			}
			
		}
	}

	protected function activityDescription($description)
	{
		return "{$description}_" . strtolower(class_basename($this));
	}

	public function recordActivity($description)
    {
        $this->activity()->create([
        	'user_id' => ($this->project ?? $this)->user->id,
        	// 'user_id' => auth()->id(),
            'description' => $description,
            'changes' => $this->activityChanges(),
            'project_id' => class_basename($this) === 'Project' ? $this->id : $this->project->id
        ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject')->latest();
    }

    public function activityChanges()
    {
        if ($this->wasChanged())
        {
            return [
                'before' => Arr::except(array_diff($this->oldAttributes, $this->getAttributes()), ['updated_at']),
                'after' => Arr::except($this->getChanges(), ['updated_at'])
            ];
        }
    }

    protected static function recordableEvents()
    {
    	if (isset(static::$recordableEvents)) {
			return static::$recordableEvents;
		}
		
		return ['created', 'updated'];
		

    }

  
}