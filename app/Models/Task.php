<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;


use App\Models\Project;

class Task extends Model
{
    use HasFactory, RecordsActivity;


    protected $fillable = ['body', 'project_id', 'completed'];
    protected $touches = ['project'];
    protected $casts = ['completed' => 'boolean'];
    protected static $recordableEvents = ['created', 'deleted'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function project()
    {
    	return $this->belongsTo(Project::class);
    }

    public function path()
    {
    	return "/projects/{$this->project->id}/tasks/{$this->id}";
    }

    public function complete()
    {
        $this->update(['completed' => true]);
        $this->recordActivity('completed_task');
    }

    public function incomplete()
    {
        $this->update(['completed' => false]);
        $this->recordActivity('incompleted_task');
    }

    // public function activity()
    // {
    //     return $this->morphMany(Activity::class, 'subject')->latest();
    // }

}
