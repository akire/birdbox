<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
// use Illuminate\Support\Arr;

use App\Models\User;
use App\Models\Task;
use App\Models\Activity;

class Project extends Model
{
    use HasFactory, RecordsActivity;

    protected $fillable = ['title', 'description', 'notes', 'user_id'];
    // protected static $recordableEvents = ['created', 'updated'];
    

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function path()
    {
    	return "/projects/{$this->id}";
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function addTask($body)
    {
    	return $this->tasks()->create(compact('body'));
    }

    public function tasks()
    {
    	return $this->hasMany(Task::class);
    }

    public function activity()
    {
        return $this->hasMany(Activity::class)->latest();
    }

    public function invite(User $user)
    {
        return $this->members()->attach($user);
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'project_members')->withTimestamps();
    }

}
