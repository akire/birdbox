<div class = "card flex flex-col mt-3">

	<h3 class = "font-bold text-xl py-4 -ml-5 border-l-4 border-blue pl-4">
		Invite a User
	</h3>

	<form action = "{{ $project->path() . '/invitations' }}" method = "POST">
		@csrf
		<div class = "mb-3">
			<input name = "email" type = "email" class = "border border-grey rounded w-full py-2 px-3" placeholder = "Email address" required>
		</div>
		<button type = "submit" class = "button-blue">Invite</button>
	</form>

	@include ('errors', ['bag' => 'invitations'])
</div>