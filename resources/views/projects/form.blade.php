

	<!-- <form class = "lg:w-1/2 lg:mx-auto bg-white p-6 md:py-12 md:px-16 rounded shadow" action = "{{ $project->path() }}" method = "POST"> -->
		
		@csrf

		
		<div class = "field mb-6">
			<label class = "label text-sm mb-2 block" for = "title">Title</label>
			<div class = "control">
				<input type = "text" class = "input bg-transparent border border-grey-light rounded p-2 text-xs w-full" name = "title" placeholder = "My next awesome project" value = "{{ $project->title }}" required>
			</div>
			<!-- @error('title')
				<p class = "error-message">{{ $message }}</p>
			@enderror -->
		</div>

		<div class = "field mb-6">
			<label class = "label text-sm mb-2 block" for = "description">Description</label>
			<div class = "control">
				<textarea name = "description" class = "textarea input bg-transparent border border-grey-light rounded p-2 text-xs w-full" placeholder = "Description" required>{{ $project->description }}</textarea>
			</div>
			<!-- @error('description')
				<p class = "error-message">{{ $message }}</p>
			@enderror -->
		</div>

		<div class = "flex justify-between items-center field">
			<button type = "submit" class = "button-blue">{{ $buttonText }}</button>

			<a href = "{{ $project->path() }}" class = "btn btn-primary">Cancel</a>
		</div>

		@if ($errors->any())

			<div class = "fieldset mt-6">
		
				@foreach ($errors->all() as $error)

					<li class = "error-message">{{ $error }}</li>
				@endforeach
			</div>
		@endif
	<!-- </form> -->
