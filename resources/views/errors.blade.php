@if ($errors->{ $bag ?? 'default' }->any())
	<ul class = "mt-5 list-reset">
		@foreach ($errors->{ $bag ?? 'default' }->all() as $error)
			<li class = "error-message">{{ $error }}</li>
		@endforeach
	</ul>
@endif