<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use App\Models\User;


class InvitationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function non_owners_may_not_invite_members()
    {
        $project = ProjectFactory::create();

        $user = User::factory()->create();

        $assertInvitationForbidden = function () use($user, $project) {
            $this->actingAs($user)
                ->post($project->path() . '/invitations')
                ->assertStatus(403);
        };

        $assertInvitationForbidden();

        $project->invite($user);

        $assertInvitationForbidden();

        // $project = ProjectFactory::create();

        // $mike = User::factory()->create();
        // $john = User::factory()->create();

        // $project->invite($mike);

        // $this->signIn($mike);

        // $this->post($project->path(). '/invitations', [
        //         'email' => $john->email
        //     ])->assertStatus(403);

        // $this->assertFalse($project->members->contains($john));

        // // $this->signIn($mike);

        // $this->actingAs($project->user)->post($project->path(). '/invitations', [
        //         'email' => $john->email
        //     ])->assertStatus(200);
        // $this->assertTrue($project->members->contains($john));

    }

    /** @test */
    public function a_project_owner_can_invite_a_user()
    {
        // $this->withoutExceptionHandling();
        $project = ProjectFactory::create();

        $user = User::factory()->create();

        $project->invite($newUser = User::factory()->create());
        $this->actingAs($project->user)->post($project->path(). '/invitations', [
                'email' => $user->email
            ])->assertRedirect($project->path());

        $this->assertTrue($project->members->contains($user));
    }

    /** @test */
    public function the_invited_email_address_must_be_associated_with_a_valid_birdboard_account()
    {
        $project = ProjectFactory::create();
        $this->actingAs($project->user)
            ->post($project->path() . '/invitations', [
                'email' => 'notregistered@mail.com'
            ])->assertSessionHasErrors([
                'email' => 'The user you are inviting must have a Birdboard account.'
            ], null, 'invitations');
    }

    /** @test */
    public function invited_users_may_update_project_details()
    {
        $project = ProjectFactory::create();
        $project->invite($newUser = User::factory()->create());
        $this->signIn($newUser);
        // $this->post(action('ProjectTasksController@store', $project), $task = ['body' => 'Foo task']);
        $this->post($project->path(). '/tasks', $task = ['body' => 'Foo task']);

        $this->assertDatabaseHas('tasks', $task);
    }
}
