<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use App\Models\Project;
use App\Models\Task;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function guests_cannot_add_tasks_to_projects()
    {
        $project = Project::factory()->create();
        $this->post($project->path() . '/tasks')->assertRedirect('login');

    }

    /** @test */
    public function only_the_owner_of_the_project_may_add_tasks()
    {
        
        $this->signIn();
        $project = Project::factory()->create();
        $this->post($project->path() . '/tasks', ['body' => 'Test task'])
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['body' => 'Test task']);

    }

     /** @test */
    public function only_the_owner_of_the_project_may_update_a_task()
    {
        
        $this->signIn();
        // $project = Project::factory()->create();
        $project = ProjectFactory::withTasks(1)->create();

        // $task = $project->addTask('test 101');
        $this->patch($project->tasks[0]->path(), ['body' => 'test 124'])
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['body' => 'test 124']);

    }

    /** @test */
    public function a_project_can_have_tasks()
    {

        // $this->signIn();

        // $project = auth()->user()->projects()->create(Project::factory()->raw());

        $project = ProjectFactory::create();

        $this->actingAs($project->user)->post($project->path() . '/tasks', ['body' => 'Test task']);

        $this->get($project->path())->assertSee('Test task');

    }

    /** @test */
    public function a_task_can_be_updated()
    {
        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->user)->patch($project->tasks->first()->path(), [
            'body' => 'changed'
        ]);

        $this->assertDatabaseHas('tasks', [
            'body' => 'changed'
        ]);

    }

    /** @test */
    public function a_task_can_be_completed()
    {
        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->user)->patch($project->tasks->first()->path(), [
            'body' => 'changed',
            'completed' => true
        ]);

        $this->assertDatabaseHas('tasks', [
            'body' => 'changed',
            'completed' => true
        ]);

    }

     /** @test */
    public function a_task_can_be_marked_as_incomplete()
    {
        $this->withoutExceptionHandling();
        
        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->user)->patch($project->tasks->first()->path(), [
            'body' => 'changed',
            'completed' => true
        ]);

        $this->actingAs($project->user)->patch($project->tasks->first()->path(), [
            'body' => 'changed',
            'completed' => false
        ]);

        $this->assertDatabaseHas('tasks', [
            'body' => 'changed',
            'completed' => false
        ]);

    }

    /** @test */
    public function a_task_requires_a_body()
    {

        // $this->signIn();
        // $project = auth()->user()->projects()->create(Project::factory()->raw());


        $project = ProjectFactory::create();

        $attributes = Task::factory()->raw(['body' => '']);

        $this->actingAs($project->user)->post($project->path() . '/tasks', $attributes)->assertSessionHasErrors('body');

    }


}
