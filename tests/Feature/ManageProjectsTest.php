<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use App\Models\Project;
use App\Models\User;

class ManageProjectsTest extends TestCase
{

    use WithFaker, RefreshDatabase;

    /** @test */
    public function guests_cannot_manage_projects()
    {
        // $this->withoutExceptionHandling(); 

        $project = Project::factory()->create();

        $this->get('/projects')->assertRedirect('login');
        $this->get('/projects/create')->assertRedirect('login');
        $this->get($project->path())->assertRedirect('login');
        $this->get($project->path(). '/edit')->assertRedirect('login');
        $this->post('/projects', $project->toArray())->assertRedirect('login');
    }

    /** @test */
    public function a_user_can_create_a_project()
    {
        // $this->withoutExceptionHandling();
        $this->signIn();

        $this->get('/projects/create')->assertStatus(200);

        $attributes = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->sentence,
            'notes' => 'General notes'
        ];

        // $attributes = Project::factory()->raw(['user_id' => auth()->id()]);


        $this->followingRedirects()->post('/projects', $attributes)
            ->assertSee($attributes['title'])
            ->assertSee($attributes['description'])
            ->assertSee($attributes['notes']);
    }

  
    /** @test */
    public function a_user_can_update_a_projects_general_notes()
    {
        // $this->withoutExceptionHandling();
        // $this->signIn();

        // $project = Project::factory()->create(['user_id' => auth()->id()]);

        $project = ProjectFactory::ownedBy($this->signIn())->create();

        $this->patch($project->path(), $attributes = ['notes' => 'test']);

        $this->assertDatabaseHas('projects', $attributes);
    }

    /** @test */
    public function a_user_can_update_their_project()
    {
        $this->withoutExceptionHandling();
        // $this->signIn();

        // $project = Project::factory()->create(['user_id' => auth()->id()]);

        $project = ProjectFactory::ownedBy($this->signIn())->create();

        $this->patch($project->path(), $attributes = ['notes' => 'test', 'title' => 'changed', 'description' => 'descriptions'])
            ->assertRedirect($project->path());

        $this->get($project->path() . '/edit')->assertOk();

        $this->assertDatabaseHas('projects', $attributes);
    }

     /** @test */
    public function a_user_can_view_their_project()
    {
        // $this->signIn();
        // $this->withoutExceptionHandling();
        // $project = Project::factory()->create(['user_id' => auth()->id()]);

        $project = ProjectFactory::ownedBy($this->signIn())->create();

        $this->get($project->path())
            ->assertSee($project->title)
            ->assertSee(substr($project->description,0,90));
    }

    /** @test */
    public function a_user_can_see_all_projects_they_have_been_invited_to_on_their_dashboard()
    {
        // $this->signIn();
        // $this->withoutExceptionHandling();
        // $project = Project::factory()->create(['user_id' => auth()->id()]);
        $project = tap(ProjectFactory::create())->invite($this->signIn());
        $this->get('/projects')->assertSee($project->title);
    }

    /** @test */
    public function unauthorized_users_cannot_delete_projects()
    {
        // $this->withoutExceptionHandling();
        $project = ProjectFactory::create();

        $this->delete($project->path())
            ->assertRedirect('/login');

        $user = $this->signIn();

        $this->delete($project->path())
            ->assertStatus(403);

        $project->invite($user);

        $this->actingAs($user)->delete($project->path())
            ->assertStatus(403);
    }

    /** @test */
    public function a_user_can_delete_a_project()
    {
        $project = ProjectFactory::create();

        $this->actingAs($project->user)->delete($project->path())
            ->assertRedirect('/projects');

        $this->assertDatabaseMissing('projects', $project->only('id'));
    }

    /** @test */
    public function an_authenticated_user_cannot_view_the_projects_of_others()
    {

        $this->signIn();

        // $this->withoutExceptionHandling();

        $project = Project::factory()->create();

        $this->get($project->path())->assertStatus(403);
    }


    /** @test */
    public function an_authenticated_user_cannot_update_the_projects_of_others()
    {

        $this->signIn();

        // $this->withoutExceptionHandling();

        $project = Project::factory()->create();

        $this->patch($project->path())->assertStatus(403);

        // $this->get($project->path())->assertStatus(403);
    }


    /** @test */
    public function a_project_requires_a_title()
    {

        $this->signIn();

        $attributes = Project::factory()->raw(['title' => '']);

        $this->post('/projects', $attributes)->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_project_requires_a_description()
    {
        $this->signIn();
        $attributes = Project::factory()->raw(['description' => '']);
        $this->post('/projects', $attributes)->assertSessionHasErrors('description');
    }

    
}
